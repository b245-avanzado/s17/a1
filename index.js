/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

function printUserInfo(){
	let userFullName = prompt("Enter your full name here: ");
	let userAge = prompt("Enter your age here: ");
	let userLocation = prompt("Enter your location here: ");
	
	console.log("Hello, " + userFullName);
	console.log("You are " + userAge + " years old.");
	console.log("You live in " + userLocation);
}

printUserInfo();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

function printFavoriteBands(){
	let favoriteBand1 = "Neck Deep";
	let favoriteBand2 = "Beyond the Horizon";
	let favoriteBand3 = "Fall Out Boy";
	let favoriteBand4 = "My Chemical Romance";
	let favoriteBand5 = "Panic! At The Disco";

	console.log("Here are your top 5 favorite bands: ")
	console.log("1. " + favoriteBand1);
	console.log("2. " + favoriteBand2);
	console.log("3. " + favoriteBand3);
	console.log("4. " + favoriteBand4);
	console.log("5. " + favoriteBand5)
}

printFavoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

function printFavoriteMovies(){
	let favoriteMovie1 = "Avengers: Endgame";
	let movieRating1 = "94";
	let favoriteMovie2 = "Spider-Man: No Way Home";
	let movieRating2 = "93";
	let favoriteMovie3 = "The Menu";
	let movieRating3 = "88%";
	let favoriteMovie4 = "Avengers: Infinity War";
	let movieRating4 = "85";
	let favoriteMovie5 = "Black Panther: Wakanda Forever";
	let movieRating5 = "84%";

	console.log("Here are your top 5 favorite movies: ");
	console.log("1. " + favoriteMovie1);
	console.log("Rotten Tomatoes Rating: " + movieRating2);
	console.log("2. " + favoriteMovie2);
	console.log("Rotten Tomatoes Rating: " + movieRating3);
	console.log("3. " + favoriteMovie3);
	console.log("Rotten Tomatoes Rating: " + movieRating4);
	console.log("4. " + favoriteMovie4);
	console.log("Rotten Tomatoes Rating: " + movieRating5);
	console.log("5. " + favoriteMovie5);
	console.log("Rotten Tomatoes Rating: " + movieRating5);
}

printFavoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

printFriends();
function printFriends(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


// console.log(friend1);
// console.log(friend2);